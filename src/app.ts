import express from 'express';

const app = express();
const port = 3000;
app.get('/', (req, res) => {
    res.send('Sullfurick lance "Piétinement" sur Golbut Majeur ! Coup Critique !');
});
app.listen(port, () => {
    return console.log(`server is listening on ${port}`);
});