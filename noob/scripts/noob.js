let myImage = document.querySelector('img');
let myHeading = document.querySelector("h1");
let myButton = document.querySelector('button');


myImage.addEventListener('click', function() {
    let mySrc = myImage.getAttribute('src');
    if (mySrc === 'images/gamer.jpg') {
        myImage.setAttribute('src', 'images/firefox2.jpg');
        myHeading.textContent = "UN GOLBUT MAJEUR APPARAÎT !!"
    } else {
        myImage.setAttribute('src', 'images/gamer.jpg');
        myHeading.textContent = "BIG NOOB CODING"
    }
});
function setUserName() {
    let myName = prompt('Veuillez saisir votre nom.');
    localStorage.setItem('nom', myName);
    myHeading.textContent = 'ACAB, camarade ' + myName;
}
if (!localStorage.getItem('nom')) {
    setUserName();
} else {
    let storedName = localStorage.getItem('nom');
    myHeading.textContent = 'Mozilla est cool, ' + storedName;
}
myButton.addEventListener('click', function() {
    setUserName();
});

