
import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, OneToOne, JoinColumn, OneToMany, ManyToOne } from "typeorm";
import { EnumRole, EnumType } from '../enums/enum';
import { GaleryPics } from "./GaleryPics";
import { ProfilePic } from "./ProfilPic";

interface welcomeStepInterface {
    step_1: Date | null;
    step_2: Date | null;
    step_3: Date | null;
    step_4: Date | null;
}

@Entity()
export class User {

    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({default: true})
    isActive: boolean;

    //TODO: replace this with real subscription when this feature will be implemented
    @Column({default: false})
    isFakeDemoSubscribed: boolean;

    @Column({
        type: "enum",
        enum: EnumRole,
        nullable: false
    })
    role: EnumRole;

    @Column({nullable: false, unique: true})
    email: string;

    @Column({nullable: false})
    password: string;

    @OneToOne(() => ProfilePic, profile_pic => profile_pic.user, {nullable: true})
    @JoinColumn()
	  profile_pic: ProfilePic;

    @OneToMany(() => GaleryPics, (galery_pics) => galery_pics.user, {nullable: true})
    @JoinColumn()
    galery_pics: GaleryPics

    @Column({nullable: true, unique: true})
    nickname: string;

    @Column({nullable: true})
    firstname: string;

    @Column({nullable: true})
    lastname: string;

    @Column({nullable: true})
    birthday: Date;

    @Column({nullable: true})
    phone: string

    @Column({
        type: "enum",
        enum: EnumType,
        nullable: true
    })
    type: EnumType;

    @Column({nullable: true})
    job_position: string

    @Column({nullable: true})
    name: string //nom entreprise

    @Column({nullable: true})
    business_name: string //raison sociale

    @Column({nullable: true})
    activity: string

    @Column({nullable: true})
    schedule: string

    @Column({nullable: true})
    siren: string

    @Column({nullable: true})
    address: string


    @Column({nullable: true})
    city: string

    @Column({nullable: true})
    zipCode: number;

    @Column({nullable: true})
    latitude: string;

    @Column({nullable: true})
    longitude: string;

    @Column({nullable: true})
    url_web: string;

    @Column({nullable: true})
    url_facebook: string;

    @Column({nullable: true})
    url_instagram: string;

    @Column({nullable: true})
    url_linkedin: string;

    @Column({nullable: true})
    url_pinterest: string;

    @Column({nullable: true})
    biography: string;

    @Column({nullable: true,default: false})
    isMatched: boolean;

    @Column({
        default: JSON.stringify({
            step_1: null,
            step_2: null,
            step_3: null,
            step_4: null,
        })
    })
    welcome_step: string;


    @Column({ nullable: true, type: 'uuid' })
    referrerId: string;

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;
}
