import {Entity, Column, PrimaryColumn, OneToOne} from "typeorm";
import {User} from "./User";

@Entity()
export class ProfilePic {
    
    @PrimaryColumn()
    id: string;
    
    @Column()
    size: string;

    @Column({nullable: false})
    userId: string;

    @Column({ type: 'json', nullable: true})
    childs: string;
    
    @OneToOne(() => User, user => user.profile_pic)
    user: User;
    
}
