
import { Entity, Column, CreateDateColumn, PrimaryColumn } from "typeorm";

import { ProfilePic } from "./ProfilPic";


@Entity()
export class UserProfilTask {

    @PrimaryColumn("uuid")
    userId: string;

    @Column({type: Boolean, default: false})
    send: boolean;

    @Column({ type: String, nullable: true })
    messageId: string;

    @CreateDateColumn()
    created_at: Date;

}
