import {Entity, Column, PrimaryColumn, OneToOne, ManyToOne, OneToMany} from "typeorm";
import {User} from "./User";

@Entity()
export class GaleryPics {
    
    @PrimaryColumn()
    id: string;
    
    @Column()
    size: string;

    @Column({nullable: false})
    userId: string;

    @Column({ type: 'json', nullable: true })
    childs: string;
    
    @ManyToOne(() => User, (user) => user.galery_pics)
    user: User;
}
