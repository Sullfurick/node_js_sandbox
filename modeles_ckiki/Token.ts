import "reflect-metadata";
import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn } from "typeorm";

export enum enumTypes {
	"CONFIRM_MAIL" = 1,
	"FORGOT_PASSWORD" = 2,
	"REFRESH_TOKEN" = 3,
	"SESSION_TOKEN" = 4
}

@Entity()
export class Token {
	@PrimaryGeneratedColumn("uuid")
	id: string;

	@Column()
	user_id: string;

	@Column({
		type: "enum",
		enum: enumTypes,
	})
	type: enumTypes;

	@Column({ type: "longtext" })
	token: string;

	@Column({
		nullable: true
	})
	expirationTime: Date;

	@CreateDateColumn()
	created_at: Date;

	@UpdateDateColumn()
	updated_at: Date;
}
