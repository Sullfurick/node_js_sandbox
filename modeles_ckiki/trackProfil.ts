import "reflect-metadata";
import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, Index } from "typeorm";


@Entity()
export class TrackProfil {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({nullable: false})
    userId: string;

    @Column({ nullable: false })
    profilId: string;

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;
}
