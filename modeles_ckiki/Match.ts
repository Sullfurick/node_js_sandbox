import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { EnumStatus } from "../enums/enum";
import { MatchProduct } from "./MatchProduct";

@Entity()
export class Match {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({nullable: false})
    userId: string;

    @Column({nullable: false})
    friendId: string;

    @Column({
        type: "enum",
        enum: EnumStatus,
        nullable: true
    })
    status: EnumStatus

    @Column({nullable: false, default: true})
    isNotified: boolean;

    @OneToMany(() => MatchProduct, (matchProduct: MatchProduct) => matchProduct.matchId)
    matchProduct: MatchProduct[];

    @CreateDateColumn()
    created_at: Date;

    @CreateDateColumn()
    updated_at: Date;
}

