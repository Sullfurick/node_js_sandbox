import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { EnumStatus } from "../enums/enum";
import { Match } from "./Match";

@Entity()
export class MatchProduct {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({nullable: false})
    name: string

    @Column({ nullable: true })
    location: string;

    @Column({ nullable: true })
    locationName: string;

    @Column({ nullable: false })
    userFrom: string;

    @Column({ nullable: false })
    userTo: string;

    @ManyToOne(() => Match, (match: Match) => match.id)
    matchId: Match;

    @Column({
        type: "enum",
        enum: EnumStatus,
        nullable: true
    })
    status: EnumStatus

    @Column()
    token: string;

    @CreateDateColumn()
    created_at: Date;

    @CreateDateColumn()
    updated_at: Date;
}

